# Dr Stache 2D

## How to play our game:

To play our game, Dr. Stache, simply use the arrow keys to move left and right as well as to jump. Use the mouse to aim while you shoot a rocket using the left mouse button to fire it towards the cursor. 

Use the rocket to trigger environmental reactions that slow down or incapacitate the enemy robots so you can avoid them as you progress through this scene of our non-lethal platformer.

And that's all there is to it! We hope you enjoy!
